import { Router } from 'express'

import { Produto as ProdutoController } from '../controller/produto'

export const produto = Router()

const controller = new ProdutoController()

produto.get('/', async (request, response) => {
    controller.list(request, response)
})

produto.get('/:id', async (request, response) => {
    controller.find(request, response)
})

produto.post('/', async (request, response) => {
    controller.add(request, response)
})

produto.put('/:id', async (request, response) => {
    controller.update(request, response)
})

produto.delete('/:id', async (request, response) => {
    controller.delete(request, response)
})