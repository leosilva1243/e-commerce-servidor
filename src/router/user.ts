import * as express from 'express'
import * as HttpStatus from 'http-status-codes'

import { User as UserController } from '../controller/user'

export const user = express.Router()

const controller = new UserController()

user.get('/', async (request, response) => {
    controller.list(request, response)
})

user.get('/:id', async (request, response) => {
    controller.find(request, response)
})

user.post('/', async (request, response) => {
    controller.add(request, response)
})

user.put('/:id', async (request, response) => {
    controller.update(request, response)
})

user.delete('/:id', async (request, response) => {
    controller.delete(request, response)
})