import { connect, ConnectionOptions } from 'mongoose'
import * as mongoose from 'mongoose'
import { app } from './app'

(<any>mongoose).Promise = global.Promise

connect('localhost:27017/e-commerce')
    .then(() => {
        console.log('conectado ao banco de dados')
        app.listen(8080, () => {
            console.log('escutando na porta 8080')
        })
    })
    .catch(console.error)
