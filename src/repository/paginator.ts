import { DocumentQuery, Document } from 'mongoose'

export class Paginator<TDocument extends Document> {
    paginate(
        entities: DocumentQuery<TDocument[], TDocument>, pagination: Pagination
    ): DocumentQuery<TDocument[], TDocument> {
        const pageSize = pagination.pageSize || 10
        const toSkip = pagination.page * pageSize
        return entities.skip(toSkip).limit(pageSize)
    }
}

export interface Pagination {
    page: number,
    pageSize?: number
}