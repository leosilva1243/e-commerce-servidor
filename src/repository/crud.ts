import { Document, Model } from 'mongoose'
import { Entity } from '../entity/entity'
import { Error } from './error'
import { Mapper } from './mapper'
import { Pagination, Paginator } from './paginator'

export class CRUD<TEntity extends Entity> {
    private readonly paginator = new Paginator<TEntity & Document>()

    constructor(
        private readonly model: Model<TEntity & Document>,
        private readonly mapper: Mapper<TEntity>
    ) { }

    async list<Order extends keyof TEntity>(
        pagination?: Pagination, filter?: Object, order?: Order
    ): Promise<TEntity[]> {
        let documentsQuery = this.model.find(filter || {})
        if (order) {
            documentsQuery = documentsQuery.sort(order)
        }
        if (pagination) {
            documentsQuery = this.paginator.paginate(documentsQuery, pagination)
        }
        const documents = await documentsQuery.exec()
        return documents.map(this.mapper.toEntity)
    }

    async find(id: number): Promise<TEntity | undefined> {
        const document = await this.model.findOne({ id: id })
        if (!document) return undefined
        return this.mapper.toEntity(document)
    }

    async add(entity: TEntity): Promise<TEntity> {
        try {
            const document = await this.mapper.toDocument(entity).save()
            return this.mapper.toEntity(document)
        }
        catch (error) {
            throw Error.duplicateKey
        }
    }

    async update(entity: TEntity): Promise<void> {
        let document = await this.model.findOne({ id: entity.id })
        if (!document) {
            throw Error.notFound
        }
        this.mapper.toDocument(entity, document)
        await document.save()
    }

    async delete(id: number): Promise<TEntity> {
        const document = await this.model.findOne({ id: id })
        if (!document) {
            throw Error.notFound
        }
        await document.remove()
        return this.mapper.toEntity(document)
    }
}