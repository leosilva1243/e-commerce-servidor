import { Produto as ProdutoEntity } from '../entity/produto'
import { Produto as ProdutoModel } from '../model/produto'
import { CRUD } from './crud'
import { DefaultMapper } from './mapper'

export class Produto extends CRUD<ProdutoEntity> {

    constructor() {
        const mapper = new DefaultMapper<ProdutoEntity>(ProdutoModel)
        super(ProdutoModel, mapper)
    }
}