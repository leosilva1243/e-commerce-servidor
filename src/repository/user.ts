import { User as UserEntity } from '../entity/user'
import { User as UserModel } from '../model/user'
import { CRUD } from './crud'
import { DefaultMapper } from './mapper'

export class User extends CRUD<UserEntity> {

    constructor() {
        const mapper = new DefaultMapper<UserEntity>(UserModel)
        super(UserModel, mapper)
    }
}