import { Entity } from './entity'

export interface Tamanho {
    altura: number
    largura: number
    profundidade: number
}

export interface Produto extends Entity {
    identificacao: string
    categoria: string
    fabricante: string
    preco: number
    peso: number
    dimensoes: Tamanho
    descricao: string
}