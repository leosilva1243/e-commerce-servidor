import { Entity } from './entity'

export interface User extends Entity {
    id: number
    username: string
    password: string
}