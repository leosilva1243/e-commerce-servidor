import * as express from 'express'

import { user } from './router/user'
import { produto } from './router/produto'

export const router = express.Router()

router.use('/users', user)
router.use('/produtos', produto)