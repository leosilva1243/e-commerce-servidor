import { Document } from 'mongoose'
import { Request, Response } from 'express'
import * as HttpStatus from 'http-status-codes'

import { CRUD as CRUDService } from '../service/crud'
import { Entity } from '../entity/entity'
import { Error as RepositoryError } from '../repository/error'

export class CRUD<TEntity extends Entity> {
    constructor(
        private readonly service: CRUDService<TEntity>
    ) { }

    async list(request: Request, response: Response) {
        const entities = await this.service.list()
        return response.json(entities)
    }

    async find(request: Request, response: Response) {
        const id = +request.params['id']
        const entity = await this.service.find(id)
        if (!entity) {
            return response
                .status(HttpStatus.NOT_FOUND)
                .json(`Entity with id ${id} not found.`)
        }
        return response.json(entity)
    }

    async add(request: Request, response: Response) {
        let entity = request.body as TEntity
        try {
            entity = await this.service.add(entity)
            return response
                .status(HttpStatus.CREATED)
                .json(entity)
        }
        catch (error) {
            if (error === RepositoryError.duplicateKey) {
                return response
                    .status(HttpStatus.BAD_REQUEST)
                    .json(`Entity has some duplicate unique key.`)
            }
            return response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json()
        }
    }

    async update(request: Request, response: Response) {
        const id = +request.params['id']
        const entity = request.body as TEntity
        if (!entity) {
            return response
                .status(HttpStatus.BAD_REQUEST)
                .json(`Request body cannot be empty.`)
        }
        if (entity.id !== id) {
            return response
                .status(HttpStatus.BAD_REQUEST)
                .json(`Entity id and route id do not match.`)
        }
        try {
            await this.service.update(entity)
            return response
                .status(HttpStatus.NO_CONTENT)
                .json()
        }
        catch (error) {
            if (error === RepositoryError.notFound) {
                return response
                    .status(HttpStatus.NOT_FOUND)
                    .json(`Entity with id ${id} not found.`)
            }
            return response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json()
        }
    }

    async delete(request: Request, response: Response) {
        const id = +request.params['id']
        try {
            const entity = await this.service.delete(id)
            return response.json(entity)
        }
        catch (error) {
            if (error === RepositoryError.notFound) {
                return response
                    .status(HttpStatus.NOT_FOUND)
                    .json(`Entity with id ${id} not found.`)
            }
            return response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json()
        }
    }
}