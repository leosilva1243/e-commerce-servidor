import { Produto as ProdutoEntity } from '../entity/produto'
import { Produto as ProdutoService } from '../service/produto'
import { CRUD } from './crud'

export class Produto extends CRUD<ProdutoEntity> {
    constructor() {
        const service = new ProdutoService()
        super(service)
    }
}