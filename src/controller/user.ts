import { User as UserEntity } from '../entity/user'
import { User as UserService } from '../service/user'
import { CRUD } from './crud'

export class User extends CRUD<UserEntity> {
    constructor() {
        const service = new UserService()
        super(service)
    }
}