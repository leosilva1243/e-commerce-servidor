import { model, Schema, Document } from 'mongoose'
import * as AutoIncrement from 'mongoose-sequence'

import { Produto as ProdutoEntity } from '../entity/produto'

const TamanhoSchema = new Schema({
    altura: Number,
    largura: Number,
    profundidade: Number
})

const schema = new Schema({
    id: {
        type: Number,
        unique: true
    },
    identificacao: String,
    categoria: String,
    fabricante: String,
    preco: Number,
    peso: Number,
    dimensoes: TamanhoSchema,
    descricao: String
})

schema.plugin(AutoIncrement, { id: 'produto_counter', inc_field: 'id' })

export const Produto = model<ProdutoEntity & Document>('Produto', schema)