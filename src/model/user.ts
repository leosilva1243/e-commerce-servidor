import { model, Schema, Document } from 'mongoose'
import * as AutoIncrement from 'mongoose-sequence'

import { User as UserEntity } from '../entity/user'

const schema = new Schema({
    id: {
        type: Number,
        unique: true
    },
    username: {
        type: String,
        unique: true
    },
    password: String
})

schema.plugin(AutoIncrement, { id: 'user_counter', inc_field: 'id' })

export const User = model<UserEntity & Document>('User', schema)