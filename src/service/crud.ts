import { Document } from 'mongoose'

import { Entity } from '../entity/entity'
import { CRUD as CrudRepository } from '../repository/crud'
import { Error } from '../repository/error'
import { Pagination } from '../repository/paginator'

export class CRUD<TEntity extends Entity> {
    constructor(
        private readonly repository: CrudRepository<TEntity>
    ) { }

    async find(id: number): Promise<TEntity | undefined> {
        return this.repository.find(id)
    }

    async list<Order extends keyof TEntity>(
        pagination?: Pagination, filter?: Object, order?: Order
    ): Promise<TEntity[]> {
        return this.repository.list(pagination, filter, order)
    }

    async add(user: TEntity): Promise<TEntity> {
        return this.repository.add(user)
    }

    async update(user: TEntity): Promise<void> {
        return this.repository.update(user)
    }

    async delete(id: number): Promise<TEntity> {
        return this.repository.delete(id)
    }
}