import { Produto as ProdutoEntity } from '../entity/produto'
import { Produto as ProdutoRepository } from '../repository/produto'
import { CRUD } from './crud'

export class Produto extends CRUD<ProdutoEntity> {
    constructor() {
        const repository = new ProdutoRepository()
        super(repository)
    }
}