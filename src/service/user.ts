import { User as UserEntity } from '../entity/user'
import { User as UserRepository } from '../repository/user'
import { CRUD } from './crud'

export class User extends CRUD<UserEntity> {
    constructor() {
        const repository = new UserRepository()
        super(repository)
    }
}